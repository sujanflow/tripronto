//
//  SelectedDestinationCell.h
//  Tripronto
//
//  Created by Tanvir Palash on 5/16/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItineraryDayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *weekDay;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
