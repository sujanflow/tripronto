//
//  SelectedDestinationCell.m
//  Tripronto
//
//  Created by Tanvir Palash on 5/16/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import "ItineraryDayCell.h"

@implementation ItineraryDayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
