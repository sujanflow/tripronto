//
//  IteneraryCommentCell.h
//  Tripronto
//
//  Created by Tanvir Palash on 4/20/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IteneraryCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *messageDetails;

@end
