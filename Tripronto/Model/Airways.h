//
//  Recipe.h
//  RecipeApp
//
//  Created by Simon on 25/12/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Airways : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *airwaysDescription;
@property (nonatomic, strong) NSString *logo;
@property (nonatomic, strong) NSString *airwaysId;


@end
