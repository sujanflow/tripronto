//
//  AboutMeViewController.h
//  Tripronto
//
//  Created by Sujan on 10/3/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutMeViewController : UIViewController



@property (weak, nonatomic) IBOutlet UITextView *aboutMeTextView;


@end
