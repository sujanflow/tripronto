//
//  EditProfileTableViewCell.h
//  Tripronto
//
//  Created by Sujan on 10/2/16.
//  Copyright © 2016 Tanvir Palash. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UITextField *editInfoTextField;


@end
